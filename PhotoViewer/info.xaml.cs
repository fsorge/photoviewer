﻿using System;
using System.IO;
using System.Windows;

namespace PhotoViewer
{
    /// <summary>
    /// Interaction logic for info.xaml
    /// </summary>
    public partial class info : Window
    {
        public info(Uri file)
        {
            InitializeComponent();

            string source = "<html><body style='font-family: Arial'>";

            FileInfo info = new FileInfo(file.AbsolutePath);
            source += "<b>Name:</b> " + info.Name
                + "<br/><b>Extension:</b> " + info.Extension
                + "<br/><b>Created:</b> " + info.CreationTime
                + "<br/><b>Modified:</b> " + info.LastWriteTime
                + "<br/><b>Accessed:</b> " + info.LastAccessTime + "<br/>";

            var directories = MetadataExtractor.ImageMetadataReader.ReadMetadata(file.AbsolutePath);
            foreach (var directory in directories)
                foreach (var tag in directory.Tags)
                    source += $"<b>{directory.Name}-{tag.Name}:</b> {tag.Description}<br/>";

            source += "</body></html>";

            browser.NavigateToString(source);
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
