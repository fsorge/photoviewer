﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;

namespace PhotoViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Uri file;

        public MainWindow()
        {
            InitializeComponent();

            toolbarVisibility(false);
            Show();

            //directory = Directory.GetCurrentDirectory();

            try
            {
                setFile(new Uri(Environment.GetCommandLineArgs()[1]));
            } catch(Exception) { }

            if (file == null)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    setFile(new Uri(openFileDialog.FileName));
                }
            }
        }
        
        private void associate()
        {
            
        }

        private string getDir()
        {
            return new FileInfo(file.AbsolutePath).Directory.FullName;
        }

        private string getName()
        {
            return new FileInfo(file.AbsolutePath).Name;
        }

        private void setFile(Uri uri)
        {
            file = uri;
            image.Source = new System.Windows.Media.Imaging.BitmapImage(file);
            setTitlebar(getName());
        }

        private void setTitlebar(string title)
        {
            Title = title + @" - Photo Viewer";
        }

        private void toolbarVisibility(bool? b=null)
        {
            if (b == null) b = toolbar.Visibility == Visibility.Collapsed;
            toolbar.Visibility = b == true ? Visibility.Visible : Visibility.Collapsed;
        }

        private void ImageButton_Click(object sender, RoutedEventArgs e)
        {
            toolbarVisibility();
        }

        private void OpenDir_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(getDir());
        }

        private void Info_Click(object sender, RoutedEventArgs e)
        {
            info window = new info(file);
            window.Show();
        }
    }
}
